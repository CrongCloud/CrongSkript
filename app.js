var express = require('express')
var app = express();
var http = require('http')
var https = require('spdy')
var fs = require('fs')
var bodyParser = require('body-parser');
var sql = require('./libs/dbtool');
var cookieSession = require('cookie-session')
var ss = require('./config/server_settings');
var SqlString = require('sqlstring');
var compression = require('compression')

app.set('view engine', 'pug');
app.set('views', './views');

if(ss.http.pretty_html == true) {
  app.locals.pretty = true;
}
app.use(compression())
app.use(function(req,res,next){
  if (!req.secure) {
    return res.redirect('https://' + req.get('host') + req.url);
  } else {
    next()
  }
});
app.use(cookieSession({
  name: 'session',
  keys: [ss.session.secret],
  maxAge: 24 * 60 * 60 * 1000
}))
app.use(function(req,res,next){
    res.removeHeader("x-powered-by");
    res.locals.session = req.session;
    res.set("Access-Control-Allow-Origin", '*');
    next();
});
app.use(bodyParser.urlencoded({extended: false}));
app.use('/public', express.static('public'));
app.use('/.well-known', express.static('.well-known'));
app.use( require('./libs/logging') );
app.use( require('./libs/pjax')() );
app.use( require('./libs/hostname')() );
app.use( require('./libs/allow_ip')() );




app.all('/', function(req, res){
  res.send('개발중이거덩요')
})




var http_server = http.createServer(app);

var ssloptions = {
   ca: fs.readFileSync('./config/ssl/ca.pem', 'utf8'),
   key: fs.readFileSync('./config/ssl/key.pem', 'utf8'),
   cert: fs.readFileSync('./config/ssl/cert.pem', 'utf8')
}
var https_server = https.createServer(ssloptions, app);

http_server.listen(ss.http.http_port, '0.0.0.0', function() {
  console.log('[Crong Skript Server] HTTP server listening on port ' + http_server.address().port);
});
https_server.listen(ss.http.https_port, '0.0.0.0', function(){
  console.log("[Crong Skript Server] HTTPS server listening on port " + https_server.address().port);
});
