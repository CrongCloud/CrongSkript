# Crong Skript
## 개요
Crong Skript(크롱 스크립트)는 Crong Cloud와 Baw_Appie의 Baw Service에 이은, 두번째 프로젝트입니다.

Crong Skript는 자신의 스크립트를 업로드하고, 팁을 공유하여, 다른 사람의 스크립트를 다운로드할 수 있는 플랫폼입니다.

Node.js, Express로 개발되어 상당히 빠른 속도를 느끼실 수 있습니다.

이젠 네이버 카페와 같은 스크립트 공유에 최적화되지 않은곳에서 Crong Skript로 넘어올때입니다!

## 시작
Crong Skript는 Baw Service 계정으로 로그인 서비스가 지원됩니다.
추후 Crong Cloud로 로그인도 추가할 예정입니다.

## Credits
Powered by Crong Cloud and Baw Service!
